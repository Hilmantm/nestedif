package com.example.nestedif;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtInputNama, txtInputTinggiBadan, txtInputBeratBadan;
    Button btnHitung;
    TextView txtTampilBMI, txtTampilHasilBMI;
    RadioButton rdPria, rdWanita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtInputBeratBadan = findViewById(R.id.txtInputBeratBadan);
        txtInputNama = findViewById(R.id.txtInputNama);
        txtInputTinggiBadan = findViewById(R.id.txtInputTinggiBadan);

        txtTampilBMI = findViewById(R.id.txtTampilBMI);
        txtTampilHasilBMI = findViewById(R.id.txtHasilBMI);

        rdPria = findViewById(R.id.radioPria);
        rdWanita = findViewById(R.id.radioWanita);

        btnHitung = findViewById(R.id.btnHitung);
        btnHitung.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                float beratBadan = Float.parseFloat(txtInputBeratBadan.getText().toString().trim());
                float tinggiBadan = Float.parseFloat(txtInputTinggiBadan.getText().toString().trim()) / 100;
                float bmi = beratBadan / (tinggiBadan * tinggiBadan);
                String hasil = "";
                if(rdPria.isChecked()) {
                    if(bmi > 27) {
                        hasil = "Obesitas";
                    } else if(bmi > 23 && bmi <= 27) {
                        hasil = "Kegemukan";
                    } else if(bmi >= 17 && bmi <= 23) {
                        hasil = "Normal";
                    } else {
                        hasil = "Kurus";
                    }
                }
                txtTampilBMI.setText("BMI : " + bmi);
                txtTampilHasilBMI.setText("Berat Badan : " + hasil);
            }
        });

    }
}
